package pl.siedlik.posts.posts.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import pl.siedlik.posts.base.ui.livedata.switchMapToFetchState
import pl.siedlik.posts.posts.repository.PostsRepository

class PostsViewModel(
  private val repository: PostsRepository
) : ViewModel() {

  private val refreshQuery = MutableLiveData<Unit>()

  val posts = refreshQuery.switchMapToFetchState { repository.getPosts() }

  fun refresh() = refreshQuery.postValue(Unit)
}

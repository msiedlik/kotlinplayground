package pl.siedlik.posts.posts.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.fragment_posts.errorView
import kotlinx.android.synthetic.main.fragment_posts.refreshLayout
import kotlinx.android.synthetic.main.fragment_posts.textView
import pl.siedlik.posts.R
import pl.siedlik.posts.base.network.error.NetworkErrorUtils
import pl.siedlik.posts.base.ui.fragment.KodeinFragment
import pl.siedlik.posts.base.ui.livedata.FetchState
import pl.siedlik.posts.base.ui.livedata.FetchState.Error
import pl.siedlik.posts.base.ui.livedata.FetchState.Progress
import pl.siedlik.posts.base.ui.livedata.FetchState.Success
import pl.siedlik.posts.posts.model.Post

class PostsFragment : KodeinFragment() {

  private val viewModel: PostsViewModel by viewModel()

  private val usersViewModel: UsersViewModel by activityViewModel()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_posts, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewModel.refresh()
    setupSwipeRefresh()
    observePostFetchState()
  }

  private fun observePostFetchState() {
    viewModel.posts.observeBy(::renderPostsFetchState)
  }

  private fun renderPostsFetchState(state: FetchState<List<Post>>) = when (state) {
    is Progress -> {
      refreshLayout.isRefreshing = true
    }
    is Success -> {
      refreshLayout.isRefreshing = false
      errorView.isVisible = false
      textView.isVisible = true
      textView.text = state.result.toString()
    }
    is Error -> {
      refreshLayout.isRefreshing = false
      textView.isVisible = false
      errorView.showAndSetMessage(NetworkErrorUtils.getErrorViewType(state.cause))
    }
  }

  private fun setupSwipeRefresh() {
    refreshLayout.setOnRefreshListener { viewModel.refresh() }
  }
}
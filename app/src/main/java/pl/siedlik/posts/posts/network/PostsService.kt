package pl.siedlik.posts.posts.network

import pl.siedlik.posts.posts.model.Post
import retrofit2.Response
import retrofit2.http.GET

interface PostsService {

  @GET("posts")
  suspend fun getPosts(): Response<List<Post>>
}
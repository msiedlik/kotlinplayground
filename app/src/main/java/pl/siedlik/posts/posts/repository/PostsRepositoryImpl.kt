package pl.siedlik.posts.posts.repository

import pl.siedlik.posts.posts.model.Post
import pl.siedlik.posts.posts.network.PostsService
import retrofit2.Response

class PostsRepositoryImpl(
  private val service: PostsService
) : PostsRepository {

  override suspend fun getPosts(): Response<List<Post>> = service.getPosts()
}
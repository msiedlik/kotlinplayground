package pl.siedlik.posts.posts.repository

import pl.siedlik.posts.posts.model.Post
import retrofit2.Response

interface PostsRepository {

  suspend fun getPosts(): Response<List<Post>>
}
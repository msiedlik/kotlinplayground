package pl.siedlik.posts.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import pl.siedlik.posts.R

class MainActivity : AppCompatActivity() {

  private lateinit var navController: NavController

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    navController = Navigation.findNavController(this, R.id.navHostFragment)
  }

  override fun onSupportNavigateUp() = NavigationUI.navigateUp(navController, null)
}

package pl.siedlik.posts.base.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import pl.siedlik.posts.posts.repository.PostsRepository
import pl.siedlik.posts.posts.repository.PostsRepositoryImpl

object RepositoryModule {

  val module = Kodein.Module("repositoryModule") {
    bind<PostsRepository>() with singleton { PostsRepositoryImpl(instance()) }
  }
}
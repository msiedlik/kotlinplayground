package pl.siedlik.posts.base.ui.view.errorview

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import pl.siedlik.posts.R

enum class ErrorViewType(@DrawableRes val icon: Int, @StringRes val message: Int) {
  DEFAULT(R.drawable.ic_error_sad_face, R.string.error_default),
  ERROR(R.drawable.ic_error_alert, R.string.error_default),
  SERVER_ERROR(R.drawable.ic_error_server, R.string.error_server),
  NO_INTERNET(R.drawable.ic_error_no_network, R.string.error_no_internet),
  EMPTY_LIST(R.drawable.ic_error_empty_box, R.string.error_empty_list)
}
package pl.siedlik.posts.base.ui.view.errorview

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.view_error.view.errorImageView
import kotlinx.android.synthetic.main.view_error.view.errorTextView
import pl.siedlik.posts.R
import pl.siedlik.posts.base.ui.view.errorview.ErrorViewType.DEFAULT

class ContentErrorView : LinearLayout {

  constructor(context: Context) : super(context) {
    initialize()
  }

  constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
    initialize()
  }

  constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
    initialize()
  }

  private fun initialize() {
    inflate(context, R.layout.view_error, this)
  }

  fun setMessage(@StringRes messageRes: Int, messageType: ErrorViewType = DEFAULT) {
    errorIcon = messageType
    errorMessage = context.getString(messageRes)
  }

  fun showAndSetMessage(messageType: ErrorViewType = DEFAULT) {
    this@ContentErrorView.isVisible = true
    errorIcon = messageType
    errorMessage = context.getString(messageType.message)
  }

  var errorMessage: CharSequence = ""
    set(value) {
      errorTextView.text = value
      field = value
    }

  var errorIcon: ErrorViewType = DEFAULT
    set(value) {
      errorImageView.setImageResource(value.icon)
      field = value
    }
}
package pl.siedlik.posts.base.network.interceptor

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(private val context: Context) : Interceptor {

  companion object {
    const val NO_INTERNET_CONNECTION = "No internet connection"
  }

  @Throws(IOException::class)
  override fun intercept(chain: Interceptor.Chain): Response {
    if (!isOnline()) {
      throw NetworkConnectivityException()
    }

    val builder = chain.request()
      .newBuilder()
    return chain.proceed(builder.build())
  }

  private fun isOnline(): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (Build.VERSION.SDK_INT < 23) {
      connectivityManager.activeNetworkInfo?.let {
        return it.isConnected && (it.type == ConnectivityManager.TYPE_WIFI || it.type == ConnectivityManager.TYPE_MOBILE)
      }
    } else {
      val network = connectivityManager.activeNetwork
      connectivityManager.getNetworkCapabilities(network)?.let {
        return it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
      }
    }
    return false
  }

  class NetworkConnectivityException : IOException(NO_INTERNET_CONNECTION)

}
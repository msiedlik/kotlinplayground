package pl.siedlik.posts.base.ui.adapter.delegates

interface BaseDelegateItem {

  val id: Any

  fun generateId(): String = "${javaClass.simpleName}$id"
}
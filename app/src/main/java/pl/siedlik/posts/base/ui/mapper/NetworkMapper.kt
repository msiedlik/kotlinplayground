package pl.siedlik.posts.base.ui.mapper

interface NetworkMapper<in U, out N> {

  fun mapToNetwork(ui: U): N

}
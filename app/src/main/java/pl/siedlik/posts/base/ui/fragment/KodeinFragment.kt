package pl.siedlik.posts.base.ui.fragment

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import pl.siedlik.posts.base.ui.viewmodel.ViewLifecycleOwner
import pl.siedlik.posts.base.ui.viewmodel.ViewModelFactory

abstract class KodeinFragment : Fragment(), KodeinAware, ViewLifecycleOwner {

  override val kodein by kodein()

  val viewModelFactory: ViewModelFactory by instance()

  inline fun <reified T : ViewModel> viewModel() =
    lazy { ViewModelProvider(this, viewModelFactory)[T::class.java] }

  inline fun <reified T : ViewModel> activityViewModel() =
    lazy { ViewModelProvider(requireActivity(), viewModelFactory)[T::class.java] }
}
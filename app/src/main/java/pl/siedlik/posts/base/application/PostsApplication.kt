package pl.siedlik.posts.base.application

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import pl.siedlik.posts.BuildConfig
import pl.siedlik.posts.base.di.RepositoryModule
import pl.siedlik.posts.base.di.ServiceModule
import pl.siedlik.posts.base.di.ViewModelModule
import pl.siedlik.posts.base.network.di.NetworkModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class PostsApplication : Application(), KodeinAware {

  override val kodein by Kodein.lazy {
    //    bind<Context>() with singleton { this@PostsApplication.applicationContext }
    import(androidXModule(this@PostsApplication))
    import(NetworkModule.module)
    import(ServiceModule.module)
    import(RepositoryModule.module)
    import(ViewModelModule.module)
  }

  override fun onCreate() {
    super.onCreate()
    initializeTimber()
  }

  private fun initializeTimber() {
    if (BuildConfig.DEBUG) {
      Timber.plant(DebugTree())
    }
  }
}
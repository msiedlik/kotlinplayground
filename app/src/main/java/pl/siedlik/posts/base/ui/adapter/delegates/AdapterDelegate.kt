package pl.siedlik.posts.base.ui.adapter.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface AdapterDelegate<T> {

  fun isForViewType(item: T): Boolean

  fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

  fun onBindViewHolder(item: T, holder: RecyclerView.ViewHolder)
}
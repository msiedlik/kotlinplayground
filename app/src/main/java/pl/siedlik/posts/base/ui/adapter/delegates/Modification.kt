package pl.siedlik.posts.base.ui.adapter.delegates

data class Modification<T : BaseDelegateItem>(val item: T, val modification: (T) -> T) {

  fun modify(): T = modification(item)
}

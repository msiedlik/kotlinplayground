package pl.siedlik.posts.base.network.di

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import pl.siedlik.posts.BuildConfig
import pl.siedlik.posts.base.network.interceptor.ConnectivityInterceptor
import pl.siedlik.posts.base.network.logger.NetworkLogger
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule {

  val module = Kodein.Module("networkModule") {

    bind() from singleton { NetworkLogger() }

    bind<OkHttpClient>() with singleton {
      OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor(instance()).also { it.level = HttpLoggingInterceptor.Level.BODY })
        .addInterceptor(ConnectivityInterceptor(instance()))
        .build()
    }

    bind<Retrofit>() with singleton {
      Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .client(instance())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    }
  }
}
package pl.siedlik.posts.base.ui.livedata

sealed class FetchState<T> {
  class Progress<T> : FetchState<T>()
  class Success<T>(val result: T) : FetchState<T>()
  class Error<T>(val cause: Throwable) : FetchState<T>()
}
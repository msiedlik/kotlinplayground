package pl.siedlik.posts.base.network.error

import pl.siedlik.posts.base.network.interceptor.ConnectivityInterceptor.NetworkConnectivityException
import pl.siedlik.posts.base.ui.view.errorview.ErrorViewType
import retrofit2.HttpException

object NetworkErrorUtils {

  fun getErrorViewType(throwable: Throwable) = when (throwable) {
    is NetworkConnectivityException -> {
      ErrorViewType.NO_INTERNET
    }
    is HttpException -> {
      when (throwable.code()) {
        in 500..550 -> ErrorViewType.SERVER_ERROR
        in 400..490 -> ErrorViewType.ERROR
        else -> ErrorViewType.DEFAULT
      }
    }
    else -> {
      ErrorViewType.DEFAULT
    }
  }
}
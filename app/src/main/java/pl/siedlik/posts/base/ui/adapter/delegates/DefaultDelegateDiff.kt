package pl.siedlik.posts.base.ui.adapter.delegates

import androidx.recyclerview.widget.DiffUtil

object DefaultDelegateDiff : DiffUtil.ItemCallback<BaseDelegateItem>() {

  override fun areItemsTheSame(oldItem: BaseDelegateItem, newItem: BaseDelegateItem) = oldItem.generateId() == newItem.generateId()

  override fun areContentsTheSame(oldItem: BaseDelegateItem, newItem: BaseDelegateItem) = oldItem == newItem
}
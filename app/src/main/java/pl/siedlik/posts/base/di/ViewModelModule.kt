package pl.siedlik.posts.base.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import pl.siedlik.posts.base.extensions.bindViewModel
import pl.siedlik.posts.base.ui.viewmodel.ViewModelFactory
import pl.siedlik.posts.posts.ui.PostsViewModel
import pl.siedlik.posts.posts.ui.UsersViewModel

object ViewModelModule {

  val module = Kodein.Module("viewModelModule") {
    bind<ViewModelFactory>() with singleton { ViewModelFactory(kodein) }
    bindViewModel<PostsViewModel>() with provider { PostsViewModel(instance()) }
    bindViewModel<UsersViewModel>() with provider { UsersViewModel(instance()) }
  }
}
package pl.siedlik.posts.base.ui.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import kotlinx.coroutines.Dispatchers
import pl.siedlik.posts.base.ui.livedata.FetchState.Error
import pl.siedlik.posts.base.ui.livedata.FetchState.Progress
import pl.siedlik.posts.base.ui.livedata.FetchState.Success
import retrofit2.HttpException
import retrofit2.Response

fun <T> unpackToFetchState(action: suspend () -> Response<T>): LiveData<FetchState<T>> = liveData(Dispatchers.IO) {
  emit(Progress())
  try {
    val response = action.invoke()
    if (response.isSuccessful) {
      emit(Success(response.body()!!))
    } else {
      throw HttpException(response)
    }
  } catch (e: Exception) {
    emit(Error(e))
  }
}

fun <T> MutableLiveData<*>.switchMapToFetchState(action: suspend () -> Response<T>): LiveData<FetchState<T>> =
  switchMap { unpackToFetchState(action) }